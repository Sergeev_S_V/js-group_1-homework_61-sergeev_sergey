import React, {Component, Fragment} from 'react';
import CountryList from "../../components/CountryList/CountryList";
import CountryData from "../../components/CountryData/CountryData";
import axios from 'axios';

class CountryBuilder extends Component {

  state = {
    countryList: [],
    currentCountry: '',
    bordersName: []
  };

  getCountryData = name => {
    let foundCountry = this.state.countryList.find(country => country.name === name);
    this.setState({currentCountry: foundCountry});
    if (foundCountry) {
      axios.get(/alpha/ + foundCountry.alpha3Code)
        .then(response => Promise.all(response.data.borders.map(border => axios.get(/alpha/ + border))))
        .then(countries => {
          let bordersName = countries.map(country => country.data.name);
          this.setState({bordersName});
        });
    }
  };

  componentDidMount() {
    axios.get('/all').then(resp => {
      this.setState({countryList: resp.data});
    })
  }

  render() {
    return(
      <Fragment>
        <CountryList list={this.state.countryList}
                     clicked={this.getCountryData}/>
        <CountryData country={this.state.currentCountry}
                     borders={this.state.bordersName}/>
      </Fragment>
    );
  }
}

export default CountryBuilder;