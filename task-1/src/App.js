import React, { Component } from 'react';
import './App.css';
import CountryBuilder from "./containers/CountryBuilder/CountryBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <CountryBuilder/>
      </div>
    );
  }
}

export default App;
