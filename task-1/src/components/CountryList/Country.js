import React from 'react';
import './Country.css';

const Country = props => {
  return(
    <div>
      <p className='Country' onClick={props.clicked}>
        {props.children}</p>
    </div>
  );
};


export default Country;